MAKALE ADI: NLP KULLANILARAK HABERLERİN YAŞ GRUPLARINA GÖRE SINIFLANDIRILMASI 
Anahtar Kelimeler: Haber Yaş Grubu Tespiti, Yaş Grubu Sözlüğü, Zemberek, Doğal Dil İşleme


Hayatın her alanında hızla gelişen teknoloji ve beraberinde getirdiği internet ağı çok kısa sürede her türlü
bilgiye ulaşmaya olanak sağlamaktadır. Bilginin yaygın fakat kirli bir hal alması karşısında, bilginin
erişebilmesinden çok doğru bilgiye ulaşma ihtiyacı önem kazanmıştır. Bilgi kirliliğinin ve sınırsız
paylaşımların sanal dünyada gezinen kitlelere (özellikle de yetişkin olmayanlar için) bir tık uzaklıkta
olması aslında bir sınırlandırılmanın gerekli olduğunu göstermektedir.
Her ne kadar insanlar kendilerini ve yakınlarını korumak üzere internet için servis sağlayıcılar tarafından
veya doğrudan kullanıcı tarafından uygulanan kişisel filtreler olsa da, bunlar açıkça genellikle cinsel
içerikli kaynaklar üzerinde yoğunlaşmıştır. Fakat internetin yaygın kullanımı beraberinde insanlara yeni
alışkanlıklar da kazandırmıştır. Buna en güzel örnek, basılı gazetelerin artık yerini elektronik haber
sitelerine bırakmış olmasıdır. Açıkçası bu durum ise, günlük yaşam içinde yer alan ve hiç de ahlaki
olmayan veya şiddet içeren haberleri de kontrol altına alabilmenin ne kadar önemli olduğunu
göstermektedir.
İnternetin haber üretimi ve sunumunu kolaylaştırıcı olanakları sayesinde, basılı gazetelere kıyasla çok
daha fazla habere yer verilebilmektedir. Bununla birlikte, gerek habercilerin daha fazla ve anlık haber
sunma kaygısı, gerekse yeterli mesleki bilgiye sahip olmamaları ve daha da önemlisi tıklanma sayısını 
(popülerlik) artırma kaygısı haberin niteliksel ve etik unsurlarının ihmal edilmesine sebep olabilmektedir.
Bu hatalar, söz konusu kullanıcının çocuk olduğu düşünüldüğünde çok daha önemli bir hal almaktadır
[5].
Bu husus göz önüne alınarak, bu çalışmada doğal dil işleme ile elektronik haberlerin uygun yaş gruplarına
göre sınıflandırılması istenmektedir. Yaş grupları belirlenirken Havighurst’ün “Gelişim Kuramı” temel
alınmıştır (Tablo 1). Bu kuramda 6 kategoriye ayrılan yaş grupları, internet kullanım yaşı öngörüsüne
göre (ilkokul çağı başlangıcı) yeniden düzenlenerek 3 kategoriye –çocukluk (6-13 yaş), ergenlik (13-18)
ve yetişkinlik (18+) olmak üzere– indirgenmiştir. 
Doğal Dil İşleme alanında, sözlük oluşturma işlemleri üzerine günümüze kadar birçok çalışma
yapılmıştır. Bu çalışmalar birçok dil için uygulansa da çoğunlukla İngilizce dili üzerine yapılmıştır.
Literatürdeki sözlük çalışmalarını, Türkçe ve diğer diller olmak üzere iki grupta incelemek faydalı
olacaktır.
Diğer diller için ilk çalışmalardan biri olan ve 1999 yılında ABD İngilizcesi için Silverman K., Anderson
V., Bellegarda J., Lenzo K. ve Naik D. tarafından geliştirilen “Victoria” adlı sözlüğün tasarımı ve
koleksiyonu tanıtılmıştır. Bu sözlük, Apple Computer'da konuşma sentezi araştırma ve geliştirmesini
desteklemek amacı ile oluşturulmuştur. Çalışmada üretilen sonuçlarda ise, böyle bir sözlüğün
oluşturulmasında gerçek dünya koşullarının çoğunu karakterize eden çarpıklıklar ve gürültü gibi
etkenlerden dolayı daha düşük sentez kalitesine neden olacağı ve böyle bir konuşma sentezi sözlüğünün
bir araya getirilmesinin çok büyük zorluklar yaratacağı kaydedilmiştir [7].
1999 yılındaki diğer bir önemli çalışma ise Riloff E. tarafından İngilizce dili için yapılan “AutoSlog” adlı
sözlüktür. AutoSlog sözlüğünde amaçlanan, ilgili MUC-4 terörizm metinlerini ve bunlara ilişkin cevap
anahtarları kullanılarak bilgi çıkarımı yapmaktır. Bu sözlük ile %98 oranında çok iyi bir sonuç elde
edilmiştir [8].
2004 yılında Tsalidis Ch., Vagelatos A. ve Orphanos G. tarafından modern Yunanca dili için bir
sözlük oluşturulmuştur. Çalışmanın yapılma amacı, modern Yunanca’nın morfolojik ve söz dizimsel
seviyelerde işlenmesi için elektronik formda bir sözlüğünün olma zorunluluğudur. Çünkü modern
Yunanca çok yönlü bir dil ve eski Yunanca'dan taşınan birçok karakteristik özelliği bulunan bir yazım
sistemidir. Bu çalışma ile modern Yunanca’yı karakterize eden ve her türlü elektronik formdaki Doğal Dil
İşlemede geçerli kılan özellikleri vurgulanmıştır [10].
2011 yılındaki bir diğer çalışmada, Këpuska V. Z. ve Rojanasthien P. İngilizce dili için konuşma grubu
oluşturmuşlardır. Bu çalışmada, film ve dizilerin DVD'lerinden konuşma grubu oluşturmak için bir veri
toplama sistemi sunulmuştur (konuşma tanıma, vurgu, perde, tonlama, duraksama analizi için).
Çalışmanın avantajı olarak DVD'lerden sözlük üretiminin, geleneksel bir konuşma grubu elde etme
yöntemine kıyasla önemli ölçüde düşük maliyetli olması verilmiştir. Buna ek olarak, verilerin
toplanmasının ve sözlüğe dönüştürülmesinin de daha kısa süre alacağı kaydedilmiştir. Çalışmanın
sonucunda sözlüğün konuşma grubu oluşturmak için yararlı ve çok yönlü olduğu gösterilmiştir [9].
Türkçe, her ne kadar geniş bir coğrafyada 60 milyon kişi tarafından anadili olarak konuşulan bir dil olsa
da Türkçe üzerindeki Doğal Dil İşleme çalışmaları ancak son 15-20 yıl içinde hız kazanmıştır [6].
Bu çalışmaların günümüzde en başarılı örneklerinden biri, çoğu Türkçe NLP çalışmalarında kullanılan,
Aktaş Y., Yılmaz İnce E. ve Çakır A.’nın çalışmalarında da yardımcı kütüphane olarak kullanılan 2007
yılında Akın A. A. ve Akın M. D. tarafından hazırlanan ve bu projede de faydalanılan “Zemberek” adlı
kütüphanedir [1]. Bu çalışmaya makalenin ileriki bölümlerinde detaylandırılarak yer verilecektir.
2014 yılında Eryiğit G. tarafından geliştirilen İTÜ Türkçe Doğal Dil İşleme Yazılım Zinciri [21],
Zemberek’in aksine açık kaynak kodlu olmayan NLP yazılımıdır. Bu yazılım Türkçe karakter
dönüştürücü (asciifier/deasciifier), sözcük ayrıştırıcı/cümle bölücü (tokenizer/sentence splitter), yazım
denetleyici (spell checker), biçim bilimsel çözümleyici/belirsizlik giderici (morphological
analyzer/disambiguator), varlık ismi tanıma (named entity recognizer) ve bağımlılık çözümlemesi
(dependency parser) gibi araçlardan oluşan bir platformdur. Bu platform, hem bir web ara yüzüne hem de
bir uygulama programlama ara yüzüne (API) sahiptir. Böylece farklı seviyelerdeki kullanıcılar bu
platformdan faydalanabilmektedir [20].
Türkçe ile ilgili çalışmalın en başarılı örneklerinden biri, 1985 yılında Princeton Üniversitesi Bilişsel
Bilimler Laboratuvarı’nda Miller tarafından başlatılmış olan ve günümüzde en yaygın olarak kullanılan
“WordNet” sözlüğüne [19], 2017 yılında Aktaş Y., Yılmaz İnce E. ve Çakır A., tarafından Türkçe dili
için sağlanan bilişim sözlüğüdür. Çalışmada bilgisayar ağ terimlerinin ontolojik tabanlı oluşturulması
işleminin otomatikleştirmesi, Türkçe dilinde kelimeler arası eş anlam yakın anlam gibi anlamsal
bağlantılara sahip sözlüklerin uygun bir şekilde bir araya getirilmesi gerçekleştirilmiştir. Bu sözlükte
140.009 adet kelime bulunmaktadır. Çalışılan alanın sadece bilgisayar ağ terimleri ile sınırlandırılması
çalışmanın başarısını oldukça arttırmıştır. Ontolojik bilgisayar ağları sözlüğüne veri girişi yapılan kelime
sayısı arttıkça başarı artmış, anlaşılmazlık azalmıştır [11].
Bu araştırmanın motivasyonunu, haber sitelerinin anlık haber yayınlama kaygısı ile haberlerin niteliksel
ve etik unsurlarını (cinsellik, küfür, argo, tecavüz, silah, şiddet) ihmal etmesi ve yayınlanan haberlerin
bazı yaş grupları tarafından okunmasının sakıncası oluşturmuştur. Bu sebeple özellikle çocukların, gerek
ahlaki ve gerekse psikolojik anlamda yayınlanan içeriklerden zarar görmemesi, örnek teşkil edip
özendirilmemesi adına, yaş gruplarına uygun haberlerin okunabilir olması gereklidir. Bu alanda
bildiğimiz kadarı ile dünyada uygulanan ilk çalışma olarak da önemlidir. Bu amaçla çalışmada mevcut
probleme çözüm olmak üzere, haberlerin ilgili yaş gruplarına uygun olarak sınıflandırılmasına yönelik
Türkçe haberler için bir model önerilmiş ve uygulanmıştır.
Bu çalışmanın ikinci bölümünde çalışmada kullanılan Doğal Dil İşleme, Terim Frekansı ve NLP
Zemberek Kütüphanesi kavramlarından bahsedilmiştir. Üçüncü bölümde belirtilen probleme uygun
olarak önerilen yöntemden ayrıntılı olarak bahsedilmiştir. Son olarak dördüncü bölümde ise önerilen
yöntemle birlikte ulaşılan sonuçlara ve sonuçların değerlendirilmesine yer verilmiştir
