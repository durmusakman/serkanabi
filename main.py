import os
import glob
import nltk
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
from gensim import corpora, models
from anahtar_kelime_cikarmak import extract_keywords

nltk.download('stopwords')
nltk.download('wordnet')
nltk.download('punkt')

# Metinleri okuma
folder_path = "makaleler/"  # Metin dosyalarının bulunduğu dizin
texts = []  # Metinleri tutacak listeyi tanımlıyoruz

for file_path in glob.glob(os.path.join(folder_path, "*.txt")): #Metinleri listeye aktarıyoruz
    with open(file_path, "r", encoding="utf-8") as file:
        text = file.read()
        texts.append(text)
# Metin ön işleme
stop_words = set(stopwords.words('turkish'))
lemmatizer = WordNetLemmatizer()

processed_texts = []
extract_keywords_with_method = []

for text in texts:
    tokens = nltk.word_tokenize(text.lower())
    cleaned_tokens = [lemmatizer.lemmatize(token) for token in tokens if token.isalnum() and token not in stop_words]
    processed_texts.append(cleaned_tokens)
    extract_keywords_with_method.append(extract_keywords(text, 10)) # Kendi yazmış olduğumuz 10 adet anaktar kelime çıkarma methodu.
# Anahtar kelime için LDA kullanımı.
dictionary = corpora.Dictionary(processed_texts)
corpus = [dictionary.doc2bow(text) for text in processed_texts]
num_topics = 50  # Çıkarılacak anahtar kelime sayısı
lda_model = models.LdaModel(corpus, num_topics=num_topics, id2word=dictionary)

# Anahtar kelimeleri çıkar
keyword_table = []
for i, text in enumerate(processed_texts):
    topic_scores = lda_model.get_document_topics(corpus[i])
    keywords = [dictionary[word_id] for word_id, _ in sorted(topic_scores, key=lambda x: x[1], reverse=True)[:num_topics]]
    real_keywords = extract_keywords_with_method[i]
    keyword_table.append({"Article": f"Metin {i+1}", "Stated Real Keywords": real_keywords, "Extract Real Keywords": keywords})

# Tabloyu yazdır
for item in keyword_table:
    print(f"Makale: {item['Article']}")
    print(f"Gerçek Anahtar Kelimeler: {item['Stated Real Keywords']}")
    print(f"Çıkarılan Anahtar Kelimeler LDA Kullanılarak: {item['Extract Real Keywords']}")
    print()


