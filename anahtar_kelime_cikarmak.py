import string
import re
from collections import Counter


def extract_keywords(text, num_keywords):
    text = text.lower()
    text = re.sub(r'[Çç]', 'c', text)
    text = re.sub(r'[Ğğ]', 'g', text)
    text = re.sub(r'[İı]', 'i', text)
    text = re.sub(r'[Öö]', 'o', text)
    text = re.sub(r'[Şş]', 's', text)
    text = re.sub(r'[Üü]', 'u', text)
    text = text.translate(str.maketrans("", "", string.punctuation))
    words = text.split()
    stop_words = ["ve", "veya", "ile", "bu", "şu", "gibi"]
    words = [word for word in words if word not in stop_words and len(word) > 2]
    word_freq = dict(Counter(words))
    sorted_keywords = sorted(word_freq, key=word_freq.get, reverse=True)
    keywords = sorted_keywords[:num_keywords]
    return keywords



